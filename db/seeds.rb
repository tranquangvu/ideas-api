100.times do
  Idea.create(
    title: Faker::Lorem.sentence,
    description: Faker::Lorem.paragraph
  )
end
