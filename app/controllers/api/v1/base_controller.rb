module Api
  module V1
    class BaseController < ActionController::API
      include Pagy::Backend

      def page_number
        params.dig(:page, :number) || 1
      end

      def page_size
        params.dig(:page, :size) || Pagy::DEFAULT[:items]
      end
    end
  end
end
